import gleam/io
import gleam/string
import gleam/int
import conductor/fsm
import conductor/glob_state

const tick_length = 1

pub fn main() {
  glob_state.init()

  set_sigint_handler(sigint)

  let state = glob_state.Idle
  let action = glob_state.StartPlay

  glob_state.set_state(state)
  glob_state.set_action(action)

  tick()
}

fn tick() -> Nil {
  let glob_state = glob_state.get()

  case glob_state.state {
    glob_state.Idle -> Nil
    glob_state.Playing(played_for: played_for, ..) ->
      io.println(
        "Tick! Played for: " <> int.to_string(played_for) <> ", pending action: " <> string.inspect(
          glob_state.pending_action,
        ),
      )
  }

  let next_state = fsm.run(glob_state.state, glob_state.pending_action)

  glob_state.set_state(next_state)
  glob_state.set_action(glob_state.Tick(seconds: tick_length))

  schedule_next_tick(fn() { tick() }, tick_length * 1000)
}

fn sigint() -> Nil {
  io.println("Got SIGINT, triggering new video...")

  let glob_state = glob_state.get()

  case glob_state.state {
    glob_state.Idle -> glob_state.set_action(glob_state.StartPlay)
    _ -> Nil
  }
}

external fn schedule_next_tick(tick: fn() -> Nil, milliseconds: Int) -> Nil =
  "" "((f, t) => globalThis.setTimeout(f, t))"

external fn set_sigint_handler(handler: fn() -> Nil) -> Nil =
  "./process_ffi.mjs" "set_sigint_handler"
