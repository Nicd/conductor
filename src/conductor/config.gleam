import gleam/int

const default_video_path = "./choochoo"

const default_vlc_bin = "vlc"

const default_play_timeout = "30"

const default_start_from_end = "300"

external type Env

pub fn video_path() {
  get_path("VIDEO_PATH", default_video_path)
}

pub fn vlc_path() {
  get_path("VLC_BIN", default_vlc_bin)
}

pub fn play_timeout() {
  let timeout_str = get_path("PLAY_TIMEOUT", default_play_timeout)

  let assert Ok(timeout_int) = int.parse(timeout_str)
  timeout_int
}

pub fn start_from_end() {
  let str = get_path("START_FROM_END", default_start_from_end)

  let assert Ok(i) = int.parse(str)
  i
}

fn get_path(key: String, default: String) {
  let path =
    get_all_env()
    |> get_from_env(key)

  case path {
    "" -> default
    _ -> path
  }
}

external fn get_from_env(env: Env, value: String) -> String =
  "" "((e, v) => e[v] || '')"

external fn get_all_env() -> Env =
  "../process_ffi.mjs" "get_env"
