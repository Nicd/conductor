import gleam/io
import gleam/string
import gleam/javascript/array.{Array}

pub external type ChildProcess

pub external type Error

external type ExecFileOptions

external type StringOrBuffer

pub type ExecFileCallback =
  fn(Error, String, String) -> Nil

pub fn kill(child: ChildProcess) -> Result(Nil, Nil) {
  case kill_internal(child) {
    True -> Ok(Nil)
    False -> Error(Nil)
  }
}

pub fn exec_file(
  command: String,
  args: List(String),
  callback: ExecFileCallback,
) {
  io.println("Running command: " <> command <> " " <> string.inspect(args))
  exec_file_internal(
    command,
    array.from_list(args),
    exec_file_options(),
    callback,
  )
}

external fn exec_file_internal(
  command: String,
  args: Array(String),
  options: ExecFileOptions,
  callback: ExecFileCallback,
) -> ChildProcess =
  "node:child_process" "execFile"

pub fn exec_file_sync(command: String, args: List(String)) -> String {
  io.println("Running command: " <> command <> " " <> string.inspect(args))
  exec_file_sync_internal(command, array.from_list(args), exec_file_options())
  |> normalise_output()
  |> string.trim()
}

external fn exec_file_sync_internal(
  command: String,
  args: Array(String),
  options: ExecFileOptions,
) -> StringOrBuffer =
  "node:child_process" "execFileSync"

external fn exec_file_options() -> ExecFileOptions =
  "" "(() => ({}))"

external fn kill_internal(child: ChildProcess) -> Bool =
  "" "(c => c.kill())"

pub external fn is_error(error: Error) -> Bool =
  "" "(e => e instanceof Error)"

external fn normalise_output(output: StringOrBuffer) -> String =
  "" "(bos => Buffer.isBuffer(bos) ? bos.toString() : bos)"
