import gleam/float
import conductor/exec

pub fn get_duration(file: String) -> Result(Int, Nil) {
  let out =
    exec.exec_file_sync(
      // Courtesy of CodeZ
      "ffprobe",
      [
        "-of",
        "default=nk=1:nw=1",
        "-select_streams",
        "v",
        "-show_entries",
        "stream=duration",
        file,
      ],
    )

  case float.parse(out) {
    Ok(f) -> Ok(float.truncate(f))
    Error(_) -> Error(Nil)
  }
}
