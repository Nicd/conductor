import gleam/list
import gleam/javascript/array.{Array}

pub fn get_files(dir: String) -> List(String) {
  dir
  |> read_dir()
  |> array.to_list()
  |> list.filter(fn(file) {
    case file {
      "." <> _rest -> False
      _ -> True
    }
  })
  |> list.map(fn(file) { resolve_path(dir, file) })
}

external fn read_dir(dir: String) -> Array(String) =
  "node:fs" "readdirSync"

external fn resolve_path(dir: String, file: String) -> String =
  "node:path" "resolve"
