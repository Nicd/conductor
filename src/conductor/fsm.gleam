import gleam/list
import gleam/io
import gleam/string
import gleam/int
import conductor/config
import conductor/fs
import conductor/vlc
import conductor/exec
import conductor/glob_state.{
  Action, EOF, Idle, None, NotStarted, Player, Playing, StartPlay, Started,
  State, Tick,
}
import conductor/ffprobe

pub fn run(state: State, action: Action) -> State {
  state
  |> move(action)
  |> do_effects(state)
}

fn move(state: State, action: Action) -> State {
  case action {
    None -> state
    other_action ->
      case state, other_action {
        Idle, StartPlay -> init_playing()
        Idle, _ -> state
        Playing(file, started_from, played_for, upcoming, player), Tick(seconds) -> {
          let total_seconds = played_for + seconds

          case total_seconds > config.play_timeout() {
            True -> {
              kill_vlc(player)
              Idle
            }
            False ->
              Playing(file, started_from, total_seconds, upcoming, player)
          }
        }
        Playing(upcoming: upcoming, played_for: played_for, ..), _ -> {
          case list.length(upcoming) {
            0 -> Idle
            _ -> {
              let assert Ok(next) = list.first(upcoming)
              let assert Ok(rest) = list.rest(upcoming)
              Playing(next, 0, played_for, rest, NotStarted)
            }
          }
        }
      }
  }
}

fn do_effects(new_state: State, prev_state: State) -> State {
  case new_state {
    Idle -> new_state
    Playing(file, started_from, played_for, upcoming, ..) -> {
      case prev_state {
        Playing(prev_file, player: prev_player, ..) if prev_file != file -> {
          kill_vlc(prev_player)

          start_playing(file, started_from, played_for, upcoming)
        }

        Idle -> {
          start_playing(file, started_from, played_for, upcoming)
        }

        _ -> new_state
      }
    }
  }
}

fn init_playing() -> State {
  let movies = fs.get_files(config.video_path())
  let movies_amount = list.length(movies)

  case movies_amount {
    0 -> panic
    _ -> Nil
  }

  let start_index = int.random(1, movies_amount)
  let assert #(start_section, rest) = list.split(movies, start_index)
  let assert Ok(first) = list.last(start_section)

  let assert Ok(duration) = ffprobe.get_duration(first)

  let max_start = duration - config.start_from_end()
  let start = int.random(0, max_start)
  Playing(
    file: first,
    started_from: start,
    played_for: 0,
    upcoming: rest,
    player: NotStarted,
  )
}

fn start_playing(
  file: String,
  started_from: Int,
  played_for: Int,
  upcoming: List(String),
) {
  let player = launch_vlc(file, started_from)
  Playing(file, started_from, played_for, upcoming, Started(player))
}

fn launch_vlc(file: String, started_from: Int) {
  vlc.play_file(
    file,
    started_from,
    fn(err, _, _) {
      io.println_error("VLC exited due to: " <> string.inspect(err))

      case exec.is_error(err) {
        False -> glob_state.set_action(EOF)
        True -> Nil
      }
    },
  )
}

fn kill_vlc(player: Player) -> Nil {
  case player {
    NotStarted -> Nil
    Started(child) -> {
      case exec.kill(child) {
        Ok(_) -> Nil
        Error(_) -> {
          io.println_error("Warning! Unable to close VLC.")
          Nil
        }
      }
    }
  }
}
