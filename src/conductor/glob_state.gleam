import conductor/exec

pub type GlobState {
  GlobState(state: State, pending_action: Action)
}

pub type Player {
  NotStarted
  Started(exec.ChildProcess)
}

pub type State {
  Idle
  Playing(
    file: String,
    started_from: Int,
    played_for: Int,
    upcoming: List(String),
    player: Player,
  )
}

pub type Action {
  None
  Tick(seconds: Int)
  EOF
  StartPlay
}

pub external fn get() -> GlobState =
  "" "(() => new GlobState(globalThis.__conductor_glob_state.state, globalThis.__conductor_glob_state.action))"

pub external fn set_state(state: State) -> Nil =
  "" "(s => globalThis.__conductor_glob_state.state = s)"

pub external fn set_action(action: Action) -> Nil =
  "" "(a => globalThis.__conductor_glob_state.action = a)"

pub external fn init() -> Nil =
  "" "(() => globalThis.__conductor_glob_state = globalThis.__conductor_glob_state || {})"
