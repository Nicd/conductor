import gleam/int
import conductor/config
import conductor/exec

pub fn play_file(
  some_file: String,
  start_from: Int,
  callback: exec.ExecFileCallback,
) {
  let start_from_str = int.to_string(start_from)
  exec_vlc(
    [
      some_file,
      "--fullscreen",
      "--play-and-exit",
      "--no-audio",
      "--start-time",
      start_from_str,
    ],
    callback,
  )
}

fn exec_vlc(args: List(String), callback: exec.ExecFileCallback) {
  exec.exec_file(config.vlc_path(), args, callback)
}
