import process from "node:process";

export function get_env() {
  return process.env;
}

export function set_sigint_handler(handler) {
  process.on("SIGINT", handler);
}
